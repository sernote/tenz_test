from selenium.webdriver.common.keys import Keys
import pytest
data_for_search = ['тензор']
index_of_picture = [0]


@pytest.mark.parametrize('text', data_for_search)
def test_first_result(app, text):
    app.log.info('Тест первого результата главного поиска. Ищем '+text)
    app.open_home_page()
    search_field = app.search.search_field()
    assert search_field.is_displayed() is True, app.log.critical('Поле поиска не отображается на странице')
    app.log.info('Вводим "'+text+'" в поле поиска')
    search_field.send_keys(text)
    assert app.search.suggests().is_displayed() is True, app.log.error('Подсказки(suggest) не выводятся')
    search_field.send_keys(Keys.ENTER)
    app.log.info('Жмем Enter, переходим к результатам')
    assert app.search.result_by_index(0).find_elements_by_tag_name('a')[0].get_attribute('href') == 'https://tensor.ru/',\
        app.log.error('Ссылка не та, возможно влезла реклама')


@pytest.mark.parametrize('index', index_of_picture)
def test_picture(app, index):
    app.log.info('Тест прямого и обратного перехода в картинках. Индекс '+str(index))
    app.open_home_page()
    picture_link = app.pictures.picture_link()
    assert picture_link.is_displayed() and picture_link.get_attribute('href') == 'https://yandex.ru/images/', \
        app.log.critical('Ссылка на Картинки отсутсвует или неверна')
    app.log.info('Переходим на страницу Картинки')
    picture_link.click()
    assert app.wd.current_url == 'https://yandex.ru/images/', app.log.error('Переход на невернную ссылку')
    first_image_link = app.pictures.image_tiser_link(index)
    app.pictures.go_image_by_index(index)
    assert app.pictures.big_picture().is_displayed() and app.wd.current_url == first_image_link,\
        app.log.error('Картинка не открылась или неверна')
    old_picture_link = app.pictures.big_picture_link()
    app.pictures.nav_next_picture()
    assert app.pictures.big_picture_link() != old_picture_link, app.log.error('Картинка не сменилась после перехода')
    app.pictures.nav_prev_picture()
    assert app.pictures.big_picture_link() == old_picture_link, \
        app.log.error('Отображается другая картинка после возврата назад')












