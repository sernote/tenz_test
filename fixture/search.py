from selenium.common import exceptions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class Searchhelper:
    def __init__(self, app):
        self.app = app

    def search_field(self):  # возвращает объект - поле поиска
        wd = self.app.wd
        log = self.app.log
        if wd.current_url != 'https://yandex.ru/':
            wd.get('https://yandex.ru/')
            log.info('Открываем главную страницу')
        try:
            search_field = wd.find_element_by_id('text')
        except exceptions.NoSuchElementException:
            log.critical('Поля поиска нет на главное странице')
        else:
            return search_field

    def suggests(self):  # возвращает объект - подсказки в поиске
        wd = self.app.wd
        log = self.app.log
        try:
            suggests = WebDriverWait(wd, 3).until(EC.visibility_of_element_located(
                (By.CLASS_NAME, 'popup__content')))
        except exceptions.TimeoutException:
            log.error('Подсказки не появились')
        else:
            return suggests

    def results(self):  # возвращает объект - список результатов поиска
        wd = self.app.wd
        log = self.app.log
        try:
            result = WebDriverWait(wd, 2).until(EC.visibility_of_element_located(
                (By.XPATH, '//ul[@aria-label="Результаты поиска"]')))
        except exceptions.TimeoutException:
            log.error('Результаты не появились')
        else:
            return result

    def result_by_index(self, index):  # возвращает объект - результат поиска по индексу
        return self.results().find_elements_by_tag_name('li')[index]

