from selenium.common import exceptions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class Picturehelper:
    def __init__(self, app):
        self.app = app

    def picture_link(self):  # возвращает объект - ссылку на Картинки на главной странице
        wd = self.app.wd
        log = self.app.log
        try:
            picture_link = wd.find_element_by_xpath('//a[@data-id="images"]')
        except exceptions.NoSuchElementException:
            log.critical('Ссылки на картинки нет на главной странице')
        else:
            return picture_link

    def image_tiser(self, index):  # возвращает объект - тизер картинки в сетке картинок
        wd = self.app.wd
        log = self.app.log
        try:
            image_tiser = wd.find_elements_by_class_name('cl-teaser__wrap')[index]
        except exceptions.NoSuchElementException:
            log.critical('Нет превью картинки с индексом '+str(index))
        else:
            return image_tiser

    def image_tiser_link(self, index):  # возвращает ссылку картинки из тизера
        return self.image_tiser(index).find_element_by_class_name('cl-teaser__link').get_attribute('href')

    def go_image_by_index(self, index):  # переход к картинке в сетке по индексу
        log = self.app.log
        log.info('Открываем первую картинку')
        self.image_tiser(index).click()

    def big_picture(self):  # возвращает объект - картинку из слайдера просмотра картинок
        wd = self.app.wd
        log = self.app.log
        try:
            big = WebDriverWait(wd, 2).until(EC.visibility_of_element_located(
                (By.CLASS_NAME, 'image__image')))
        except exceptions.TimeoutException:
            log.error('Полная картинка не открылась')
        else:
            return big

    def big_picture_link(self):
        return self.big_picture().get_attribute('src')

    def nav_next_picture(self):  # переход к следующей картинке в слайдере
        wd = self.app.wd
        log = self.app.log
        log.info('Переходим к следующей картинке')
        try:
            next_but = WebDriverWait(wd, 2).until(EC.visibility_of_element_located(
                (By.XPATH, '//div[@title="Следующая"]')))
        except exceptions.TimeoutException:
            log.error('Не обнаружена кнопка перехода к следующей странице')
        else:
            next_but.click()

    def nav_prev_picture(self):  # переход к предыдущей картинке в слайдере
        wd = self.app.wd
        log = self.app.log
        log.info('Переходим к предыдущей картинке')
        try:
            prev_but = WebDriverWait(wd, 2).until(EC.visibility_of_element_located(
                (By.XPATH,'//div[@title="Предыдущая"]')))
        except exceptions.TimeoutException:
            log.error('Не обнаружена кнопка перехода к следующей странице')
        else:
            prev_but.click()
