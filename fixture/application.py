from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.support.wait import WebDriverWait
from fixture.search import Searchhelper
from fixture.pictures import Picturehelper
import logging

logging.basicConfig(level=logging.INFO, filename='log.log', filemode='w',
                    format='%(asctime)s - %(levelname)s - %(message)s')


class Application:
    def __init__(self):
        self.wd = webdriver.Chrome()
        self.wd.implicitly_wait(2)
        self.exceptions = exceptions
        self.waits = WebDriverWait
        self.search = Searchhelper(self)
        self.pictures = Picturehelper(self)
        self.log = logging
        self.log.info('Запуск тестов')

    def open_home_page(self):
        wd = self.wd
        if wd.current_url != 'https://yandex.ru/':
            wd.get('https://yandex.ru/')
            self.log.info('Открываем главную страницу')

    def destroy(self):
        self.log.info('Тесты завершены')
        self.wd.quit()